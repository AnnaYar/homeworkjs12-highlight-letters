// Теоретичні питання

// 1. Чому для роботи з input не рекомендується використовувати клавіатуру?
// Т.як текст в поле input може бути вставлений за допомогою кліків мишки або голосовим введенням.
// При цьому клавіатура ніяк не буде задіяна. Тому відслідковування введення буде некоректним.

//     Завдання

/*Реалізувати функцію підсвічування клавіш. Завдання має бути виконане на чистому Javascript 
без використання бібліотек типу jQuery або React.

Технічні вимоги:
У файлі index.html лежить розмітка для кнопок.
Кожна кнопка містить назву клавіші на клавіатурі
Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися 
в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - 
вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. 
Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову 
стає чорною.*/


// Рішення за допомогою event.key

// const buttons = document.querySelectorAll('.btn');

// document.addEventListener('keydown', function (event) {
//     const keyUp = event.key.toUpperCase();
  
//     buttons.forEach(button => {
//         if (keyUp === button.textContent.toUpperCase()) {
//             button.classList.add('btn-active')
//         } else {
//             button.classList.remove('btn-active');
//         }
//     });
// });


// Рішення за допомогою event.code

const buttons = document.querySelectorAll('.btn');

document.addEventListener('keydown', function (event) {
   
    buttons.forEach(button => {
        const key = event.code.startsWith('Key') ? `Key${button.textContent.toUpperCase()}` : `${button.textContent}`;

        if ( (event.code === key ) ) {
            button.classList.add('btn-active')
        } else {
            button.classList.remove('btn-active');
        }
    });
}); 
